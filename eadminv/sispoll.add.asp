<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 'pageProtege = True %>
<!--#include file="../configuracoes/configuracoes.asp" -->
<html>
<head>
<title>ADMIN - SISPOLL - ADD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<% If Trim(Request.QueryString("add")) = "" Then %>
ADD P1<br><br>
<% If pollAddErro = "sim" Then %>Erro encontrado! Confira para poder continuar!<% End If %>
<form name="pollAddP1" action="?add=p2" method="post">
QUAL A PERGUNTA: <input name="pollPerg" type="text" id="pollPerg" size="50" maxlength="255"><br>
QUANTAS RESPOSTAS: 
<select name="pollQResp" id="pollQResp">
  <option value="1" selected>1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
  <option value="6">6</option>
  <option value="7">7</option>
  <option value="8">8</option>
  <option value="9">9</option>
  <option value="10">10</option>
  <option value="11">11</option>
  <option value="12">12</option>
  <option value="13">13</option>
  <option value="14">14</option>
  <option value="15">15</option>
</select><br>
<input type="submit" name="pollOk" value="Confirmar">
<input type="submit" name="pollCancela" value="Cancelar">
</form>
<% ElseIf Trim(Request.QueryString("add")) = "p2" Then %>
<%
	pollQResp = Trim(Request.Form("pollQResp"))
	pollPerg = Trim(Request.Form("pollPerg"))
	pollPubData = Day(Date)&"/"&Month(Date)&"/"&Year(Date)
	pollPubHora = FormatDateTime(Now(),vbLongTime)
	
	If Len(Trim(pollPerg)) = 0 Then
		Response.Redirect("sispoll.add.asp?pollAddErro=sim")
	End If
	
	Set pollAdd = Server.CreateObject("ADODB.Recordset")
	pollAdd.Open "pergunta", conn, 3, 3
	pollAdd.AddNew
		pollAdd("pergunta") = pollPerg
		pollAdd("pubdata") = pollPubData
		pollAdd("pubhora") = pollPubHora
	pollAdd.Update 
	pollIdPerg = pollAdd("id")
	pollAdd.Close
	Set pollAdd = nothing
%>
<br><br>ADD P2<br><br>
<form name="pollAddP1" action="?add=p3" method="post">
<input name="pollQResp" type="hidden" id="pollQResp" value="<%=pollQResp%>">
PERGUNTA: <%=pollPerg%><input name="pollIdPerg" type="hidden" id="pollIdPerg" value="<%=pollIdPerg%>"><br>
<% For iPoll = 1 to pollQResp %>
RESPOSTA <%=iPoll%>: <input name="pollResp<%=iPoll%>" type="text" id="pollResp<%=iPoll%>" size="50" maxlength="25"><br>
<% Next %>
<input type="submit" name="pollOk" value="Concluir">
<input type="submit" name="pollCancela" value="Cancelar">
</form>
<% ElseIf Trim(Request.QueryString("add")) = "p3" Then %>
<%
	pollQResp = Trim(Request.Form("pollQResp"))
	pollIdPerg = Trim(Request.Form("pollIdPerg"))
	pollVotos = 0
	
	For iPoll = 1 to pollQResp
	Set pollAdd = Server.CreateObject("ADODB.Recordset")
	pollAdd.Open "resposta", conn, 3, 3
	pollAdd.AddNew
		pollAdd("pergunta") = pollIdPerg
		pollAdd("resposta") = Request.Form("pollResp"&iPoll)
		pollAdd("votos") = pollVotos
	pollAdd.Update 
	pollAdd.Close
	Set pollAdd = nothing
	Next
%>
<br><br>ADD P3<br><br>
Sua enquete ficar� assim:<br>
<script language="JavaScript" src="../sispoll/mostra.script.asp?id=<%=pollIdPerg%>" type="text/JavaScript"></script><br>
<br>
Para inserir esta enquete, basta copiar e colar o seguinte c�digo:<br>
<textarea name="pollCOD" cols="50" rows="3" id="pollCOD"><script language="JavaScript" src="../sispoll/mostra.script.asp?id=<%=pollIdPerg%>" type="text/JavaScript"></script></textarea>
<br><br><a href="sispoll.asp">VOLTAR</a><br>
<% End If %>
<% If logLogado = True Then %><br><a href="logoff.asp">LOGOFF</a><% Else %><br><a href="login.asp">LOGIN</a><% End If %>
</body>
</html>
